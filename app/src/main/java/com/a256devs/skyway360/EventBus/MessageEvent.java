package com.a256devs.skyway360.EventBus;


public class MessageEvent {

    public final Messages message;
    public final Object link;

    public MessageEvent(Messages message, Object link) {
        this.message = message;
        this.link = link;
    }

    //EventBus.getDefault().post(new MessageEvent(FILE_FOR_CHAT, file));



}
