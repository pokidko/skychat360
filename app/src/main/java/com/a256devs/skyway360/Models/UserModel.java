package com.a256devs.skyway360.Models;


public class UserModel {
    String id;
    String nickname;
    boolean isConnected;

    public void setId(String id) {
        this.id = id;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public String getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public boolean isConnected() {
        return isConnected;
    }
}
