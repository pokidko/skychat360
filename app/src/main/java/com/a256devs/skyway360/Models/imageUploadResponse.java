
package com.a256devs.skyway360.Models;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class ImageUploadResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("authorized")
    @Expose
    private Integer authorized;

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The authorized
     */
    public Integer getAuthorized() {
        return authorized;
    }

    /**
     * @param authorized The authorized
     */
    public void setAuthorized(Integer authorized) {
        this.authorized = authorized;
    }

}

