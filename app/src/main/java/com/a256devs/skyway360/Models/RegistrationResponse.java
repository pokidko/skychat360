package com.a256devs.skyway360.Models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class RegistrationResponse {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("osType")
    @Expose
    private String osType;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("success")
    @Expose
    private Integer success;

    /**
     *
     * @return
     *     The error
     */
    public String getError() {
        return error;
    }

    /**
     *
     * @param error
     *     The error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     *
     * @return
     *     The userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @param userName
     *     The userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     *
     * @return
     *     The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     *     The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     *     The osType
     */
    public String getOsType() {
        return osType;
    }

    /**
     *
     * @param osType
     *     The osType
     */
    public void setOsType(String osType) {
        this.osType = osType;
    }

    /**
     *
     * @return
     *     The userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     *
     * @param userType
     *     The userType
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     *
     * @return
     *     The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     *
     * @param success
     *     The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

}