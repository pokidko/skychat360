package com.a256devs.skyway360.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Activity.SettingsActivity;
import com.a256devs.skyway360.R;
import com.a256devs.skyway360.REST.ApiRequests;
import com.a256devs.skyway360.Models.LoginResponse;


import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.a256devs.skyway360.Activity.SettingsActivity.API_SERVER_URL;
import static com.a256devs.skyway360.Activity.SettingsActivity.CHAT_FRAGMENT_TAG;
import static com.a256devs.skyway360.Activity.SettingsActivity.CHAT_REGISTRATION_FRAGMENT_TAG;
import static com.a256devs.skyway360.Activity.SettingsActivity.isLog;

public class ChatAuthorizationFragment extends SettingsFragment implements View.OnClickListener {

    Button mRegisterButton;
    Button mLoginButton;
    EditText userNameEditText;
    EditText passwordEditText;


    MainActivity mainActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.authorization_fragment, container, false);

        mRegisterButton = (Button) rootView.findViewById(R.id.authorization_register_button);
        mLoginButton = (Button) rootView.findViewById(R.id.authorization_login_button);
        userNameEditText = (EditText) rootView.findViewById(R.id.auth_user_name_et);
        passwordEditText = (EditText) rootView.findViewById(R.id.authorization_input_password);
        mRegisterButton.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);
        mainActivity = (MainActivity) getActivity();
//        mSocket.on("login", onLogin);


        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.authorization_register_button:
                mainActivity.showFragment(CHAT_REGISTRATION_FRAGMENT_TAG);
                break;
            case R.id.authorization_login_button:
                attemptLogin();
                break;
        }
    }

    //SocketIo Chat section


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v("flow", "onDestroy on Chat Auth fragment");
//        mSocket.off("login", onLogin);
    }

    /**
     * Attempts to sign in the account specified by the login form.
     * If there are form errors (invalid username, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        Log.v("flow", "Login attempt");
        // Reset errors.
        userNameEditText.setError(null);
        passwordEditText.setError(null);
        // Store values at the time of the login attempt.
        String username = userNameEditText.getText().toString().trim();
        String pas = passwordEditText.getText().toString().trim();

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            userNameEditText.setError(getString(R.string.error_field_required));
            userNameEditText.requestFocus();
            return;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            passwordEditText.setError(getString(R.string.error_field_required));
            passwordEditText.requestFocus();
            return;
        }

        mainActivity.setChatUserName(username);
        mainActivity.setChatUserPassword(pas);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiRequests api = retrofit.create(ApiRequests.class);
        HashMap<String, String> params = new HashMap<>();
        params.put("userName", username);
        params.put("password", pas);
        Call call = api.login(params);

        call.enqueue(new Callback<LoginResponse>() {
                         @Override
                         public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                             if (response.isSuccessful()) {
                                 LoginResponse lr = response.body();
                                 if ((lr.getSuccess() != null) && (lr.getSuccess() == 1)) {
                                     if ((lr.getUserType() != null)&&(lr.getUserType() == 1)) {
                                      if (isLog) Log.v("flow","userType == 1");
                                         mainActivity.setSettingsVisibility(true);
                                         mainActivity.setUserType(true);
                                     } else {
                                         mainActivity.setUserType(false);
                                         mainActivity.setSettingsVisibility(false);
                                     }
                                     mainActivity.saveUserNameAndPasswordToShared();
                                     mainActivity.showFragment(CHAT_FRAGMENT_TAG);
                                 } else {
                                     AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                     builder.setCancelable(false);
                                     builder.setMessage(getString(R.string.incorrect_chat_login));
                                     builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                         @Override
                                         public void onClick(DialogInterface dialog, int which) {

                                         }
                                     });
                                     builder.create().show();
                                 }
                             } else {
                                 Log.v("flow", "response not success");
                             }
                         }

                         @Override
                         public void onFailure(Call<LoginResponse> call, Throwable t) {
                             Log.v("flow", "nFailure " + t.toString());
                         }
                     }
        );
    }

    @Override
    public void onResume() {
        MainActivity activity = (MainActivity) getActivity();
        activity.setFragmentTitle(SettingsActivity.CHAT_AUTHORIZATION_FRAGMENT_TAG);
        super.onResume();
    }
}