package com.a256devs.skyway360.Fragments;

import android.support.v4.app.Fragment;
import android.view.View;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.EventBus.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class SettingsFragment extends Fragment implements View.OnClickListener {
    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {

        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    protected void setThisListeners(View... views) {
        for (View v : views)
            v.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }

    @Subscribe
    public void onMessageEvent(MessageEvent event) {

    }

}
