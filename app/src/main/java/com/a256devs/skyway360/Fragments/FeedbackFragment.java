package com.a256devs.skyway360.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Activity.SettingsActivity;
import com.a256devs.skyway360.R;

public class FeedbackFragment extends SettingsFragment implements View.OnClickListener {

    Button mSendButtonFeedback;
    EditText message;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.feedback_fragment, container, false);

        mSendButtonFeedback = (Button) rootView.findViewById(R.id.feedback_send_button);
        mSendButtonFeedback.setOnClickListener(this);

        message = (EditText) rootView.findViewById(R.id.feedback_text);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(intent.EXTRA_TEXT, message.getText().toString());
        intent.putExtra(intent.EXTRA_SUBJECT, "Feedback");
        intent.putExtra(intent.EXTRA_EMAIL, new String[]{"skyway360@yandex.ru"});
        if (intent.resolveActivity(getActivity().getPackageManager()) != null)
            startActivity(intent);

    }
    @Override
    public void onResume() {
        MainActivity activity = (MainActivity) getActivity();
        activity.setFragmentTitle(SettingsActivity.FEEDBACK_FRAGMENT_TAG);
        super.onResume();
    }

}
