package com.a256devs.skyway360.Fragments.ChatGroupsFragment;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Models.UserModel;
import com.a256devs.skyway360.R;

import java.util.ArrayList;

import static com.a256devs.skyway360.Activity.SettingsActivity.CHAT_CONVERSATION_TAG;

public class PrivateTabRvAdapter extends RecyclerView.Adapter {
    private Context context;
    protected MainActivity mainActivity;
    private ArrayList<PrivateTabModel> privateRoomsArrayList;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.private_tab_rv_item, parent, false);

        return new mLocViewHolder(v);
    }

    public static class mLocViewHolder extends RecyclerView.ViewHolder {
        TextView userNameTv;
        TextView newMessage;
        ImageView stateIv;
        RelativeLayout mainLayout;

        public mLocViewHolder(View itemView) {
            super(itemView);
            newMessage = (TextView) itemView.findViewById(R.id.new_message_private_item);
            userNameTv = (TextView) itemView.findViewById(R.id.private_tab_user_name);
            stateIv = (ImageView) itemView.findViewById(R.id.private_tab_image_view);
            mainLayout = (RelativeLayout) itemView.findViewById(R.id.main_layout);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mLocViewHolder mHolder = (mLocViewHolder) holder;
        final PrivateTabModel model = privateRoomsArrayList.get(position);
        mHolder.userNameTv.setText(model.getRoomNick2());
        if (model.isNewMessages()) mHolder.newMessage.setVisibility(View.VISIBLE);
        else mHolder.newMessage.setVisibility(View.GONE);
        boolean isOnLine = false;
        for (UserModel user : mainActivity.getUserList())
            if (user.getNickname().equals(model.getRoomNick2())) isOnLine = user.isConnected();
        if (isOnLine)
            mHolder.stateIv.setImageDrawable(context.getResources().getDrawable(R.drawable.round_8dp_green));
        else
            mHolder.stateIv.setImageDrawable(context.getResources().getDrawable(R.drawable.round_8dp_gray));
        mHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.currentChatRoom = model.getRoomName();
                model.setNewMessages(false);
                mainActivity.setRoomPrivate(true);
                mainActivity.showFragment(CHAT_CONVERSATION_TAG);
            }
        });

    }


    @Override
    public int getItemCount() {
        return privateRoomsArrayList.size();
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setActivity(MainActivity act) {
        this.mainActivity = act;
    }

    public void setData(ArrayList<PrivateTabModel> incomingData) {
        privateRoomsArrayList = incomingData;
    }
}
