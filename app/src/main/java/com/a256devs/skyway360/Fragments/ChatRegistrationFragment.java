package com.a256devs.skyway360.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Activity.SettingsActivity;
import com.a256devs.skyway360.R;
import com.a256devs.skyway360.REST.ApiRequests;
import com.a256devs.skyway360.Models.RegistrationResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.a256devs.skyway360.Activity.SettingsActivity.API_SERVER_URL;
import static com.a256devs.skyway360.Activity.SettingsActivity.CHAT_FRAGMENT_TAG;

public class ChatRegistrationFragment extends SettingsFragment implements View.OnClickListener {

    EditText mUserName;
    EditText mEmail;
    EditText mPassword;
    EditText mPasswordConfirm;
    Button mSend;

    MainActivity mainActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.registration_fragment, container, false);

        mUserName = (EditText) rootView.findViewById(R.id.auth_user_name_et);
        mEmail = (EditText) rootView.findViewById(R.id.registration__e_mail_et);
        mPassword = (EditText) rootView.findViewById(R.id.registration_input_password);
        mPasswordConfirm = (EditText) rootView.findViewById(R.id.registration_confirm_password);

        mSend = (Button) rootView.findViewById(R.id.registration_send_button);
        mSend.setOnClickListener(this);

        mainActivity = (MainActivity) getActivity();


        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.registration_send_button:
                if (check()) {
                    regUser();
                }
                break;
        }

    }
//   (!mPassword.getText().toString().equals(mPasswordConfirm.getText().toString())) {

    public boolean check() {
        mUserName.setError(null);
        mPassword.setError(null);
        String username = mUserName.getText().toString().trim();
        String pas = mPassword.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            mUserName.setError(getString(R.string.error_field_required));
            mUserName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(pas)) {
            mPassword.setError(getString(R.string.error_field_required));
            mPassword.requestFocus();
            return false;
        } else if (!mPassword.getText().toString().equals(mPasswordConfirm.getText().toString())) {
            Toast.makeText(getContext(), getString(R.string.error_field_required), Toast.LENGTH_LONG);
            return false;
        }
        mainActivity.setChatUserName(username);
        mainActivity.setChatUserPassword(pas);
        return true;
    }

    private void regUser() {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiRequests api = retrofit.create(ApiRequests.class);
        HashMap<String, String> params = new HashMap<>();
        params.put("userName", mUserName.getText().toString().trim());
        params.put("email", mEmail.getText().toString().trim());
        params.put("password", mPassword.getText().toString().trim());
        params.put("osType", "android");
        params.put("userType", "0");

        Call call = api.register(params);

        call.enqueue(new Callback<RegistrationResponse>() {
                         @Override
                         public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                             if (response.isSuccessful()) {


                                 RegistrationResponse rr = response.body();
                                 if ((rr.getSuccess() != null) && (rr.getSuccess() == 1)) {
                                     mainActivity.saveUserNameAndPasswordToShared();
                                     mainActivity.showFragment(CHAT_FRAGMENT_TAG);
                                 } else {
                                     AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                     builder.setCancelable(false);
                                     if (rr.getError() != null) builder.setMessage(rr.getError());
                                     else builder.setMessage(getString(R.string.can_t_register));
                                     builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                         @Override
                                         public void onClick(DialogInterface dialog, int which) {

                                         }
                                     });
                                     builder.create().show();
                                 }
                             } else {
                                 Log.v("flow", "registration not success");
                             }
                         }

                         @Override
                         public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                             Log.v("flow", "registration Failure " + t.toString());
                         }
                     }
        );
    }
    @Override
    public void onResume() {
        MainActivity activity = (MainActivity) getActivity();
        activity.setFragmentTitle(SettingsActivity.CHAT_REGISTRATION_FRAGMENT_TAG);
        super.onResume();
    }
}
