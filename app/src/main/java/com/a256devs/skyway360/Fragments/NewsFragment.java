package com.a256devs.skyway360.Fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Activity.SettingsActivity;
import com.a256devs.skyway360.R;


public class NewsFragment extends SettingsFragment implements View.OnClickListener {

    ImageView mYouTubeButton;
    ImageView mFaceBookButton;
    ImageView mInstagramButton;
    ImageView mTwitterButton;
    ImageView mVkButton;
    ImageView mGooglePlusButton;
    ImageView mSiteButton;

    WebView myWebView;
    ProgressDialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_fragment, container, false);

        mYouTubeButton = (ImageView) rootView.findViewById(R.id.you_tube_button);
        mFaceBookButton = (ImageView) rootView.findViewById(R.id.face_book_button);
        mInstagramButton = (ImageView) rootView.findViewById(R.id.instagram_button);
        mTwitterButton = (ImageView) rootView.findViewById(R.id.twitter_button);
        mVkButton = (ImageView) rootView.findViewById(R.id.vk_button);
        mGooglePlusButton = (ImageView) rootView.findViewById(R.id.google_plus_button);
        mSiteButton = (ImageView) rootView.findViewById(R.id.site_button);

        mYouTubeButton.setOnClickListener(this);
        mFaceBookButton.setOnClickListener(this);
        mInstagramButton.setOnClickListener(this);
        mTwitterButton.setOnClickListener(this);
        mVkButton.setOnClickListener(this);
        mGooglePlusButton.setOnClickListener(this);
        mSiteButton.setOnClickListener(this);

        myWebView = (WebView) rootView.findViewById(R.id.web_view);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new MyWebViewClient());
        myWebView.loadUrl("https://vk.com/skyway_official");
        dialog = ProgressDialog.show(getActivity(), "", "Please wait, Loading Page...", true);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.you_tube_button:
                myWebView.loadUrl("https://www.youtube.com/channel/UC-NcJ4R7_V8W_3nVkCAswbQ");
                break;
            case R.id.face_book_button:
                myWebView.loadUrl("https://www.facebook.com/groups/RSWsystems/");
                break;
            case R.id.instagram_button:
                myWebView.loadUrl("https://www.instagram.com/skyway_future/?hl=ru");
                break;
            case R.id.twitter_button:
                myWebView.loadUrl("https://twitter.com/rswsyst?lang=ru");
                break;
            case R.id.vk_button:
                myWebView.loadUrl("https://vk.com/skyway_official");
                break;
            case R.id.google_plus_button:
                myWebView.loadUrl("https://plus.google.com/+SKYWAYORG");
                break;
            case R.id.site_button:
                myWebView.loadUrl("http://skyway360.com/");
                break;
        }
    }

    @Override
    public void onResume() {
        MainActivity activity = (MainActivity) getActivity();
        activity.setFragmentTitle(SettingsActivity.NEWS_FRAGMENT_TAG);
        super.onResume();
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            dialog.dismiss();
        }

    }

}
