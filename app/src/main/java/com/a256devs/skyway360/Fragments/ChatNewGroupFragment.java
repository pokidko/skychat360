package com.a256devs.skyway360.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Activity.SettingsActivity;
import com.a256devs.skyway360.EventBus.MessageEvent;
import com.a256devs.skyway360.Fragments.SettingsFragment;
import com.a256devs.skyway360.Models.ImageUploadResponse;
import com.a256devs.skyway360.R;
import com.a256devs.skyway360.REST.ApiRequests;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.a256devs.skyway360.Activity.SettingsActivity.API_SERVER_URL;
import static com.a256devs.skyway360.Activity.SettingsActivity.isLog;


public class ChatNewGroupFragment extends SettingsFragment {
    String TAG = "ChatNewGroupFragment";
    RelativeLayout imageLayout;
    TextView imageTextTv;
    ImageView imageIv;
    EditText groupName;
    EditText groupDescription;


    String newGroupFilePath;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.chat_create_group_fragment, container, false);
        imageLayout = (RelativeLayout) rootView.findViewById(R.id.new_group_image_rl);
        imageTextTv = (TextView) rootView.findViewById(R.id.new_group_image_tv);
        imageIv = (ImageView) rootView.findViewById(R.id.new_group_image_iv);
        groupName = (EditText) rootView.findViewById(R.id.chat_title_group);
        groupDescription = (EditText) rootView.findViewById(R.id.chat_description_group);

        imageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity) getActivity();
                activity.selectImageForNewChat();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        if (isLog) Log.v("flow", "in the onResume ChatNewsGroupFragment");

        MainActivity activity = (MainActivity) getActivity();
        activity.setFragmentTitle(SettingsActivity.CHAT_NEW_ROOM_TAG);
        activity.showMenuItems(SettingsActivity.CHAT_NEW_ROOM_TAG, true);

        if (activity.newGroupFilePath !=null) {
            newGroupFilePath = activity.newGroupFilePath;
            if (isLog)
                Log.v("flow", "in the ChatNewGroupFragment FILE_FOR_CHAT case; isNewGroup == true");
            Picasso.with(getContext()).load("file://" + newGroupFilePath).into(imageIv);
            imageTextTv.setVisibility(View.GONE);

        }

        activity.newGroupFilePath = null;

        super.onResume();
    }

    @Override
    public void onStop() {
        MainActivity activity = (MainActivity) getActivity();
        activity.showMenuItems(SettingsActivity.CHAT_NEW_ROOM_TAG, false);
        super.onStop();
    }

    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        switch (event.message) {

            case MENU_OK_PRESSED:
                Log.v("flow", "in the OK ");
                CreateNewGroup(newGroupFilePath, groupName.getText().toString(), groupDescription.getText().toString());
                break;
        }
    }

    void CreateNewGroup(String FilePath, final String newGroupName, final String newGroupDescription) {
        final MainActivity activity = (MainActivity) getActivity();
        if (activity.isNetConnection()) {
            activity.showProgressDialog("Creating New Group");
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiRequests api = retrofit.create(ApiRequests.class);

            File file = new File(FilePath);


            // create RequestBody instance from file
            final RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("file", file.getName(), requestFile);

            // add another part within the multipart request
            String descriptionString = "file";
            RequestBody description =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), descriptionString);

            // finally, execute the request
            Call<ImageUploadResponse> call = api.upload(description, body);

            call.enqueue(new Callback<ImageUploadResponse>() {
                @Override
                public void onResponse(Call<ImageUploadResponse> call,
                                       Response<ImageUploadResponse> response) {
                    activity.hideProgressDialog();
                    if (isLog) Log.v("flow", "success" + response.body().toString());
                    if (response.isSuccessful()) {
                        ImageUploadResponse iR = response.body();
                        if ((iR.getSuccess() != null) && (iR.getSuccess() == 1)) {
                            if (iR.getUrl() != null) {
                                Log.v("flow", "Send message: " + newGroupName + iR.getUrl() + newGroupDescription);
                                activity.getSocket().emit("newRoom", newGroupName, newGroupDescription, iR.getUrl());
                                activity.onBackPressed();
                            }
                        } else {
                            Toast.makeText(getContext(), "Image didn't uploaded", Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "response isSuccessful for ImageUploadResponse not success");
                            activity.onBackPressed();

                        }
                    } else {
                        Toast.makeText(getContext(), "Image didn't uploaded", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "response for ImageUploadResponse not success");
                        activity.onBackPressed();

                    }
                }

                @Override
                public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                    activity.hideProgressDialog();
                    Toast.makeText(getContext(), "Image didn't uploaded", Toast.LENGTH_SHORT).show();
                    Log.e("Upload error:", t.getMessage());
                }
            });
        }
    }

}
