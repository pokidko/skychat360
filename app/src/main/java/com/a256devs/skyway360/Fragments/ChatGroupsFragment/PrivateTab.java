package com.a256devs.skyway360.Fragments.ChatGroupsFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.EventBus.MessageEvent;
import com.a256devs.skyway360.R;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.a256devs.skyway360.Activity.SettingsActivity.isLog;


public class PrivateTab extends Fragment {
    protected View mView;
    protected RecyclerView recyclerView;
    protected PrivateTabRvAdapter privateTabRvAdapter;
    private Socket mSocket;
    MainActivity mainActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.chat_private_fragment, null);
        recyclerView = (RecyclerView) mView.findViewById(R.id.chat_private_rv);

        mainActivity = (MainActivity) getActivity();
        mSocket = mainActivity.getSocket();


        //Creating rv Adapter
        privateTabRvAdapter = new PrivateTabRvAdapter();
        privateTabRvAdapter.setActivity((MainActivity) getActivity());
        privateTabRvAdapter.setContext(getContext());
        privateTabRvAdapter.setData(new ArrayList<PrivateTabModel>());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(privateTabRvAdapter);
        return mView;
    }


    @Override
    public void onResume() {
        mSocket.on("roomListPrivates", onRoomListPrivates);
        super.onResume();
    }

    @Override
    public void onStop() {
        mSocket.off("roomListPrivates", onRoomListPrivates);
        super.onStop();
    }

    private Emitter.Listener onRoomListPrivates = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (args[0] != null) {
                        Object obj = args[0];
                        Log.v("flow", "Private room obj " + obj.toString());
                        try {
                            mainActivity.privateRoomList = mainActivity.parsePrivateGroupsList(args[0]);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        privateTabRvAdapter.setData(mainActivity.privateRoomList);
                        privateTabRvAdapter.notifyDataSetChanged();
                    }
                }
            });
        }
    };

    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        switch (event.message) {
            case NEW_PRIVATE_CHAT_MESSAGE:
                Integer position = (Integer) event.link;
                if (isLog) Log.v("flow","got NEW_PRIVATE_CHAT_MESSAGE in PrivateTab with position : " + position);
                privateTabRvAdapter.notifyItemChanged(position);
                break;
        }
    }

}
