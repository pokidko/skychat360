package com.a256devs.skyway360.Fragments.ChatGroupsFragment;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.a256devs.skyway360.Activity.SettingsActivity.CHAT_CONVERSATION_TAG;

public class GroupTabRvAdapter extends RecyclerView.Adapter {
    protected MainActivity mainActivity;
    private ArrayList<GroupTabModel> groupsRoomsArrayList;
    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_tab_rv_item, parent, false);

        return new mLocViewHolder(v);
    }

    private static class mLocViewHolder extends RecyclerView.ViewHolder {
        TextView newMessage;
        TextView groupNameTv;
        TextView groupDescriptionTv;
        ImageView picIv;
        RelativeLayout mainLayout;

        private mLocViewHolder(View itemView) {
            super(itemView);
            newMessage = (TextView) itemView.findViewById(R.id.new_message_group_item);
            groupNameTv = (TextView) itemView.findViewById(R.id.group_tab_name_text_view);
            groupDescriptionTv = (TextView) itemView.findViewById(R.id.group_tab_description_text_view);
            picIv = (ImageView) itemView.findViewById(R.id.group_tab_image_view);
            mainLayout = (RelativeLayout) itemView.findViewById(R.id.main_layout);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mLocViewHolder mHolder = (mLocViewHolder) holder;
        final GroupTabModel model = groupsRoomsArrayList.get(position);
        if (model.isNewMessages()) mHolder.newMessage.setVisibility(View.VISIBLE);
        else mHolder.newMessage.setVisibility(View.GONE);
        mHolder.groupNameTv.setText(model.getRoomName());
        mHolder.groupDescriptionTv.setText(model.getRoomDescription());
        Picasso.with(context).load(model.getRoomIconUrl()).into(mHolder.picIv);
        mHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.currentChatRoom = model.getRoomName();
                model.setNewMessages(false);
                mainActivity.setRoomPrivate(false);
                mainActivity.showFragment(CHAT_CONVERSATION_TAG);
            }
        });

    }


    @Override
    public int getItemCount() {
        return groupsRoomsArrayList.size();
    }


    public void setActivity(MainActivity act) {
        this.mainActivity = act;
    }

    public void setData(ArrayList<GroupTabModel> incomingData) {
        groupsRoomsArrayList = incomingData;
    }
}
