package com.a256devs.skyway360.Fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Activity.SettingsActivity;
import com.a256devs.skyway360.R;


public class AuthorizationSkyWayFragment extends SettingsFragment {

    ProgressDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.authorization_sky_way_fragment, container, false);

        WebView myWebView = (WebView) rootView.findViewById(R.id.web_view_authorization);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new MyWebViewClient());
        myWebView.loadUrl("https://account.rsw-systems.com/auth/login");
        dialog = ProgressDialog.show(getActivity(), "", "Please wait, Loading Page...", true);

        return rootView;
    }

    @Override
    public void onResume() {
        MainActivity activity = (MainActivity) getActivity();
        activity.setFragmentTitle(SettingsActivity.AUTHORIZATION_SKY_WAY_FRAGMENT_TAG);
        super.onResume();
    }

    private class MyWebViewClient extends WebViewClient {



        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            dialog.dismiss();
        }

    }
}
