package com.a256devs.skyway360.Fragments.ChatConversationFragment;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Activity.SettingsActivity;
import com.a256devs.skyway360.Fragments.SettingsFragment;
import com.a256devs.skyway360.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.a256devs.skyway360.Activity.SettingsActivity.isLog;


public class ChatConversationFragment extends SettingsFragment {
    protected RecyclerView recyclerView;
    protected View mView;
    private Socket mSocket;
    MainActivity mainActivity;
    ChatConversationRvAdapter chatConversationRvAdapter;
    private ArrayList<MessageModel> chatHistoryArrayList;

    private TextView sendTextView;
    private EditText textMessageEditText;
    private ImageView sendImage;


    String TAG = "ChatConversationTAG";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.chat_conversation_fragment, container, false);
        recyclerView = (RecyclerView) mView.findViewById(R.id.chat_group_rv);
        sendTextView = (TextView) mView.findViewById(R.id.chat_send_button);
        textMessageEditText = (EditText) mView.findViewById(R.id.text_message_edit_text);
        sendImage = (ImageView) mView.findViewById(R.id.chat_image_button);
        setThisListeners(sendTextView, sendImage);

        mainActivity = (MainActivity) getActivity();
        mSocket = mainActivity.getSocket();

        chatHistoryArrayList = new ArrayList<>();
        chatConversationRvAdapter = new ChatConversationRvAdapter();
        chatConversationRvAdapter.setActivity((MainActivity) getActivity());
        chatConversationRvAdapter.setContext(getContext());
        chatConversationRvAdapter.setData(chatHistoryArrayList);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setStackFromEnd(true);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(chatConversationRvAdapter);


        return mView;
    }

    @Override
    public void onResume() {
        if (isLog) Log.v("flow", "chatRoom: onResume");
        MainActivity activity = (MainActivity) getActivity();
        activity.setFragmentTitle(SettingsActivity.CHAT_CONVERSATION_TAG);
        activity.showMenuItems(SettingsActivity.CHAT_CONVERSATION_TAG, true);

        if (isLog)
            Log.v("flow", "chatRoom:  " + mainActivity.getCurrentChatRoom() + "TAG:" + TAG);
        mSocket.emit("joinRoom", mainActivity.getChatUserName(), mainActivity.getCurrentChatRoom());
        mSocket.on("chatHistory", chatHistory);
        mSocket.emit("chatHistory", mainActivity.getCurrentChatRoom());
        mSocket.on("newChatMessage", newChatMessage);
        super.onResume();
    }


    @Override
    public void onStop() {
        if (isLog) Log.v("flow", "chatRoom: onStop");
        MainActivity activity = (MainActivity) getActivity();
        activity.showMenuItems(SettingsActivity.CHAT_CONVERSATION_TAG, false);
        mSocket.emit("leaveRoom", mainActivity.getChatUserName(), mainActivity.getCurrentChatRoom());
        mSocket.off("newChatMessage", newChatMessage);
        mSocket.off("chatHistory", chatHistory);
        super.onStop();
    }

    private Emitter.Listener chatHistory = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        chatHistoryArrayList = mainActivity.parseChatHistory(args[0]);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.v("flow", "Chat history length: " + chatHistoryArrayList.size());
                    chatConversationRvAdapter.setData(chatHistoryArrayList);
                    chatConversationRvAdapter.notifyDataSetChanged();
                    scrollToBottom();
                }
            });
        }
    };


    private Emitter.Listener newChatMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Object obj = args[0];
                    if (isLog) Log.v("flow", "Got new chat message: " + obj.toString());
                    JSONObject json = (JSONObject) args[0];
                    chatHistoryArrayList.add(mainActivity.parseMessageFromJSON(json));
                    chatConversationRvAdapter.notifyItemInserted(chatHistoryArrayList.size() - 1);
                    scrollToBottom();
                }
            });
        }
    };

    private void scrollToBottom() {
        recyclerView.scrollToPosition(chatConversationRvAdapter.getItemCount() - 1);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        Log.v("flow", "in on Click");
        switch (v.getId()) {
            case R.id.chat_send_button:
                if (isLog)
                    Log.v("flow", "Send message_blue: " + mainActivity.getChatUserName() + textMessageEditText.getText() + mainActivity.getCurrentChatRoom());
                if (textMessageEditText.getText().length() > 0)
                    mSocket.emit("chatMessage", mainActivity.getChatUserName(), textMessageEditText.getText(), mainActivity.getCurrentChatRoom(), "text");
                textMessageEditText.setText("");
                break;
            case R.id.chat_image_button:
                mainActivity.selectImageForChat();
                break;
        }
    }

}
