package com.a256devs.skyway360.Fragments.UserListFragment;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Models.UserModel;
import com.a256devs.skyway360.R;

import java.util.ArrayList;

public class UserListRvAdapter extends RecyclerView.Adapter {
    protected MainActivity mainActivity;
    private ArrayList<UserModel> users;
    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_rv_item, parent, false);

        return new mLocViewHolder(v);
    }

    private static class mLocViewHolder extends RecyclerView.ViewHolder {
        TextView userNameTv;
        ImageView stateIv;
        RelativeLayout mainLayout;

        private mLocViewHolder(View itemView) {
            super(itemView);
            userNameTv = (TextView) itemView.findViewById(R.id.user_list_user_name);
            stateIv = (ImageView) itemView.findViewById(R.id.user_list_image_view);
            mainLayout = (RelativeLayout) itemView.findViewById(R.id.main_layout);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mLocViewHolder mHolder = (mLocViewHolder) holder;
        final UserModel model = users.get(position);
        mHolder.userNameTv.setText(model.getNickname());
        if (model.isConnected())
            mHolder.stateIv.setImageDrawable(context.getResources().getDrawable(R.drawable.round_8dp_green));
        else
            mHolder.stateIv.setImageDrawable(context.getResources().getDrawable(R.drawable.round_8dp_gray));
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    public void setActivity(MainActivity act) {
        this.mainActivity = act;
    }

    public void setData(ArrayList<UserModel> incomingData) {
        users = incomingData;
    }
}