package com.a256devs.skyway360.Fragments.ChatGroupsFragment;


public class PrivateTabModel {
    private String roomName;
    private String roomNick1;
    private String roomNick2;
    private boolean isOnline;
    private boolean isNewMessages = false;

    public boolean isNewMessages() {
        return isNewMessages;
    }

    public void setNewMessages(boolean newMessages) {
        isNewMessages = newMessages;
    }



    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomNick1() {
        return roomNick1;
    }

    public void setRoomNick1(String roomNick1) {
        this.roomNick1 = roomNick1;
    }

    public String getRoomNick2() {
        return roomNick2;
    }

    public void setRoomNick2(String roomNick2) {
        this.roomNick2 = roomNick2;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }
}
