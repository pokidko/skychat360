package com.a256devs.skyway360.Fragments.UserListFragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Activity.SettingsActivity;
import com.a256devs.skyway360.Fragments.SettingsFragment;
import com.a256devs.skyway360.R;


public class UserListFragment extends SettingsFragment {
    MainActivity mainActivity;
    protected View mView;
    UserListRvAdapter userListRvAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.chat_userlist_fragment, container, false);
        RecyclerView recyclerView = (RecyclerView) mView.findViewById(R.id.user_list_rv);
        mainActivity = (MainActivity) getActivity();
        userListRvAdapter = new UserListRvAdapter();
        userListRvAdapter.setContext(getContext());
        userListRvAdapter.setActivity((MainActivity) getActivity());
        userListRvAdapter.setData(mainActivity.getUserList());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(userListRvAdapter);
        return mView;
    }

    @Override
    public void onResume() {
        MainActivity activity = (MainActivity) getActivity();
        activity.setFragmentTitle(SettingsActivity.CHAT_USERS_LIST_TAG);
        super.onResume();
    }
}
