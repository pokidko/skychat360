package com.a256devs.skyway360.Fragments.ChatGroupsFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Activity.SettingsActivity;
import com.a256devs.skyway360.EventBus.MessageEvent;
import com.a256devs.skyway360.Fragments.SettingsFragment;
import com.a256devs.skyway360.R;

import org.greenrobot.eventbus.Subscribe;

public class ChatGroupsFragment extends SettingsFragment implements View.OnClickListener {
    MainActivity mainActivity;
    ViewPager viewPager;
    TabLayout tabLayout;
    private ChatGroupsFragmentViewPagerAdapter chatGroupsFragmentViewPagerAdapter;
    private GroupTab groupTab;
    private PrivateTab privateTab;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        mainActivity.connectSocket();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.chat_fragment, container, false);

        viewPager = (ViewPager) rootView.findViewById(R.id.chat_view_pager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.chat_tab_layout);
        //Creating Tabs
        groupTab = new GroupTab();
        privateTab = new PrivateTab();
        //Create and Setup View Pager
        chatGroupsFragmentViewPagerAdapter = new ChatGroupsFragmentViewPagerAdapter(getChildFragmentManager());
        chatGroupsFragmentViewPagerAdapter.addFragment(groupTab, getString(R.string.groups_tab_name));
        chatGroupsFragmentViewPagerAdapter.addFragment(privateTab, getString(R.string.private_tab_name));
        viewPager.setAdapter(chatGroupsFragmentViewPagerAdapter);
        //Setting Up Tab Layout
        tabLayout.setupWithViewPager(viewPager);
        return rootView;
    }

    @Override
    public void onResume() {
        mainActivity.setFragmentTitle(SettingsActivity.CHAT_FRAGMENT_TAG);
        mainActivity.showMenuItems(SettingsActivity.CHAT_FRAGMENT_TAG, true);
        if (mainActivity.isSocketConnected()) {
            mainActivity.refreshPrivateTab();
            mainActivity.refreshGroupsTab();
        }
        super.onResume();


    }

    @Override
    public void onStop() {
        MainActivity activity = (MainActivity) getActivity();
        activity.setFragmentTitle(SettingsActivity.CHAT_FRAGMENT_TAG);
        activity.showMenuItems(SettingsActivity.CHAT_FRAGMENT_TAG, false);
        super.onStop();
    }


    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        switch (event.message) {
            case SOCKET_CONNECTED:
                mainActivity.connectUser();
                mainActivity.refreshGroupsTab();
                mainActivity.refreshPrivateTab();
                break;
        }
    }


}
