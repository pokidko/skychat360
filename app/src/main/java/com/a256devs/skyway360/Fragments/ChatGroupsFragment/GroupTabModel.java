package com.a256devs.skyway360.Fragments.ChatGroupsFragment;

public class GroupTabModel {
    String roomName;
    String roomDescription;
    String roomIconUrl;
    private boolean isNewMessages = false;

    public boolean isNewMessages() {
        return isNewMessages;
    }

    public void setNewMessages(boolean newMessages) {
        isNewMessages = newMessages;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public void setRoomIconUrl(String roomIconUrl) {
        this.roomIconUrl = roomIconUrl;
    }

    public void setRoomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
    }

    public String getRoomName() {
        return roomName;
    }

    public String getRoomDescription() {
        return roomDescription;
    }

    public String getRoomIconUrl() {
        return roomIconUrl;
    }
}
