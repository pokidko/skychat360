package com.a256devs.skyway360.Fragments.ChatConversationFragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.Fragments.ChatGroupsFragment.PrivateTabModel;
import com.a256devs.skyway360.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.a256devs.skyway360.Activity.SettingsActivity.CHAT_CONVERSATION_TAG;
import static com.a256devs.skyway360.Activity.SettingsActivity.isLog;

public class ChatConversationRvAdapter extends RecyclerView.Adapter {
    protected MainActivity mainActivity;
    private ArrayList<MessageModel> messages;
    private Context context;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_conversation_rv_item, parent, false);

        return new mLocViewHolder(v);
    }

    private static class mLocViewHolder extends RecyclerView.ViewHolder {
        TextView chatMessageDate;
        TextView userName;
        TextView message_blue;
        TextView message_gray;
        ImageView imageView;
        LinearLayout linearLayout;

        private mLocViewHolder(View itemView) {
            super(itemView);
            chatMessageDate = (TextView) itemView.findViewById(R.id.chat_message_date);
            userName = (TextView) itemView.findViewById(R.id.chat_name_text_view);
            message_blue = (TextView) itemView.findViewById(R.id.chat_blue_message_text_view);
            message_gray = (TextView) itemView.findViewById(R.id.chat_gray_message_text_view);
            imageView = (ImageView) itemView.findViewById(R.id.chat_image_image_view);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.main_layout);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mLocViewHolder mHolder = (mLocViewHolder) holder;
        final MessageModel model = messages.get(position);

        Date date = new Date();
        try {
            date = new SimpleDateFormat("MM/dd/yyyy, hh:mm:ss a").parse(model.getCurrentDateTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.text.DateFormat timeFormat = DateFormat.getTimeFormat(context);
        java.text.DateFormat longDateFormat = DateFormat.getLongDateFormat(context);
        mHolder.chatMessageDate.setText(longDateFormat.format(date) + " " + timeFormat.format(date));

        if (model.getMessageType().equals("text")) {
            mHolder.imageView.setVisibility(View.GONE);
        }
        if (!mainActivity.getChatUserName().equals(model.getClientNickname())) {
            mHolder.linearLayout.setGravity(Gravity.LEFT);

            mHolder.message_blue.setVisibility(View.VISIBLE);
            mHolder.message_gray.setVisibility(View.GONE);


            mHolder.userName.setText(model.getClientNickname());
            mHolder.message_blue.setText(model.getMsg());

            // For creating private room
            if (!mainActivity.isRoomPrivate()) {
                mHolder.userName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(context.getResources().getString(R.string.go_to_private_message));
                        builder.setPositiveButton(R.string.go_to_private_yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                boolean isNewRoomNeeds = true;
                                String roomName = new String();
                                for (PrivateTabModel privateRoom : mainActivity.getPrivateRoomList()) {
                                    if (privateRoom.getRoomNick2().equals(model.getClientNickname())) {
                                        isNewRoomNeeds = false;
                                        roomName = privateRoom.getRoomName();
                                    }
                                }
                                if (isNewRoomNeeds) {
                                    if (isLog) Log.v("flow", "Need New Private room");
                                    mainActivity.createNewPrivateRoom(model.getClientNickname());
                                } else {
                                    if (isLog) Log.v("flow", "Room exist");
                                    mainActivity.setRoomPrivate(true);
                                    mainActivity.setCurrentChatRoom(roomName);
                                    mainActivity.showFragment(CHAT_CONVERSATION_TAG);
                                }
                            }
                        });
                        builder.setNegativeButton(R.string.go_to_private_no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                });
            }
        } else {
            mHolder.linearLayout.setGravity(Gravity.RIGHT);

            mHolder.message_blue.setVisibility(View.GONE);
            mHolder.message_gray.setVisibility(View.VISIBLE);

            mHolder.userName.setText(model.getClientNickname());
            mHolder.message_gray.setText(model.getMsg());

        }


        if (model.getMessageType().equals("photo")) {
            mHolder.imageView.setVisibility(View.VISIBLE);
            mHolder.message_blue.setVisibility(View.GONE);
            mHolder.message_gray.setVisibility(View.GONE);

            Picasso.with(context).load(model.getMsg()).into(mHolder.imageView);
            mHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    LayoutInflater inflater = mainActivity.getLayoutInflater();
                    View dialog_view = inflater.inflate(R.layout.chat_image_dialog, null);
                    ImageView dialog_im = (ImageView) dialog_view.findViewById(R.id.chat_dialog_iv);
                    Picasso.with(context).load(model.getMsg()).into(dialog_im);
                    builder.setView(dialog_view)
                            .setPositiveButton(R.string.nav_ok_button, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return messages.size();
    }


    public void setActivity(MainActivity act) {
        this.mainActivity = act;
    }

    public void setData(ArrayList<MessageModel> incomingData) {
        messages = incomingData;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
