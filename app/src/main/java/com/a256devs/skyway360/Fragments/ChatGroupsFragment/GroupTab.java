package com.a256devs.skyway360.Fragments.ChatGroupsFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.EventBus.MessageEvent;
import com.a256devs.skyway360.R;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.a256devs.skyway360.Activity.SettingsActivity.CHAT_NEW_ROOM_TAG;
import static com.a256devs.skyway360.Activity.SettingsActivity.isLog;
import static com.a256devs.skyway360.EventBus.Messages.NEW_GROUP_CHAT_MESSAGE;

public class GroupTab extends Fragment {
    protected View mView;
    protected RecyclerView recyclerView;
    private Socket mSocket;
    GroupTabRvAdapter groupTabRvAdapter;
    MainActivity mainActivity;
    FloatingActionButton fab;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.chat_group_fragment, null);
        recyclerView = (RecyclerView) mView.findViewById(R.id.chat_group_rv);

        fab = (FloatingActionButton) mView.findViewById(R.id.chat_group_fb);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showFragment(CHAT_NEW_ROOM_TAG);

            }
        });
        mainActivity = (MainActivity) getActivity();

        if (mainActivity.isFabVisibility()) fab.setVisibility(View.VISIBLE);
        else fab.setVisibility(View.GONE);

        mSocket = mainActivity.getSocket();

        //Creating rv Adapter
        groupTabRvAdapter = new GroupTabRvAdapter();
        groupTabRvAdapter.setContext(getContext());
        groupTabRvAdapter.setActivity((MainActivity) getActivity());
        groupTabRvAdapter.setData(new ArrayList<GroupTabModel>());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(groupTabRvAdapter);
        return mView;
    }

    @Override
    public void onResume() {
        mSocket.on("roomList", onRoomList);
        super.onResume();
    }

    @Override
    public void onStop() {
        mSocket.off("roomList", onRoomList);
        super.onStop();
    }

    private Emitter.Listener onRoomList = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Object obj = args[0];
                    Log.v("flow", "Group: " + obj.toString());
                    try {
                        mainActivity.groupsRoomList = mainActivity.parseGroupsList(args[0]);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    groupTabRvAdapter.setData(mainActivity.groupsRoomList);
                    groupTabRvAdapter.notifyDataSetChanged();
                }
            });
        }
    };

    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        switch (event.message) {
            case NEW_GROUP_CHAT_MESSAGE:
                Integer position = (Integer) event.link;
                if (isLog)
                    Log.v("flow", "got NEW_GROUP_CHAT_MESSAGE in GroupTab with position : " + position);
                groupTabRvAdapter.notifyItemChanged(position);
                break;
        }
    }

}
