package com.a256devs.skyway360.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.a256devs.skyway360.Activity.MainActivity;
import com.a256devs.skyway360.R;


public class SetTitleDialog extends DialogFragment {
    EditText title;
    EditText description2;

    public Dialog onCreateDialog(Bundle saveInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_set_title, null);
        title = (EditText) dialog.findViewById(R.id.dialog_title);
        description2 = (EditText) dialog.findViewById(R.id.dialog_title_description2);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final MainActivity mainActivity = (MainActivity) getActivity();

        builder.setPositiveButton(R.string.nav_save_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mainActivity.setCoverTitleString(title.getText().toString());
                mainActivity.setDescription2(description2.getText().toString());

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });


        builder.setView(dialog);
        title.setText(mainActivity.getCoverTitleString());
        description2.setText(mainActivity.getDescription2());
        return builder.create();
    }
}
