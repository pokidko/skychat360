package com.a256devs.skyway360.Activity;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.a256devs.skyway360.Fragments.ChatAuthorizationFragment;
import com.a256devs.skyway360.Fragments.AuthorizationSkyWayFragment;
import com.a256devs.skyway360.Fragments.ChatConversationFragment.ChatConversationFragment;
import com.a256devs.skyway360.Fragments.ChatConversationFragment.MessageModel;
import com.a256devs.skyway360.Fragments.ChatNewGroupFragment;
import com.a256devs.skyway360.Fragments.ChatGroupsFragment.ChatGroupsFragment;
import com.a256devs.skyway360.Fragments.ChatGroupsFragment.PrivateTabModel;
import com.a256devs.skyway360.Fragments.FeedbackFragment;
import com.a256devs.skyway360.Fragments.NewsFragment;
import com.a256devs.skyway360.Fragments.ChatRegistrationFragment;
import com.a256devs.skyway360.Fragments.RegistrationSkyWayFragment;
import com.a256devs.skyway360.Fragments.ChatGroupsFragment.GroupTabModel;
import com.a256devs.skyway360.Fragments.UserListFragment.UserListFragment;
import com.a256devs.skyway360.Models.UserModel;
import com.a256devs.skyway360.R;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {
    public static final boolean isLog = true;
    public static final boolean isChatHistoryLog = false;


    String TAG = "SettingsActivityTAG";
    public static final String NEWS_FRAGMENT_TAG = "Лента новостей";
    public static final String CHAT_FRAGMENT_TAG = "SkyWay360 чат";
    public static final String CHAT_CONVERSATION_TAG = "Chat";
    public static final String CHAT_USERS_LIST_TAG = "он-лайн пользователи";
    public static final String CHAT_NEW_ROOM_TAG = "Создать групу";
    public static final String CHAT_AUTHORIZATION_FRAGMENT_TAG = "Авторизация в SkyWay360 Chat";
    public static final String CHAT_REGISTRATION_FRAGMENT_TAG = "Регистрация в SkyWay360 Chat";
    public static final String FEEDBACK_FRAGMENT_TAG = "Обратная связь";
    public static final String AUTHORIZATION_SKY_WAY_FRAGMENT_TAG = "Авторизация";
    public static final String REGISTRATION_SKY_WAY_FRAGMENT_TAG = "Регистрация";
    //Retrofit constants
    public static final String API_SERVER_URL = "http://liudy9k.net/api/";
    //Socket.io const
    public static final String CHAT_SERVER_URL = "http://liudy9k.net:3000";

    public String chatUserName;
    public String chatUserPassword;
public boolean userType;
    public String chatUserId;
    public String currentChatRoom;
    private boolean isRoomPrivate;

    public ArrayList<UserModel> userList; //ArrayList for userList
    public ArrayList<GroupTabModel> groupsRoomList;
    public ArrayList<PrivateTabModel> privateRoomList;

    public void showFragmentWithoutBack(String fragmentTag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;
        switch (fragmentTag) {
            case NEWS_FRAGMENT_TAG:
                fragment = new NewsFragment();
                break;
        }
        fragmentTransaction.replace(R.id.main_activity_container, fragment);
        fragmentTransaction.commit();
    }

    public void showFragment(String fragmentTag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;
        switch (fragmentTag) {
            case NEWS_FRAGMENT_TAG:
                fragment = new NewsFragment();
                break;
            case CHAT_FRAGMENT_TAG:
                fragment = new ChatGroupsFragment();
                break;
            case CHAT_AUTHORIZATION_FRAGMENT_TAG:
                fragment = new ChatAuthorizationFragment();
                break;
            case CHAT_REGISTRATION_FRAGMENT_TAG:
                fragment = new ChatRegistrationFragment();
                break;
            case FEEDBACK_FRAGMENT_TAG:
                fragment = new FeedbackFragment();
                break;
            case AUTHORIZATION_SKY_WAY_FRAGMENT_TAG:
                fragment = new AuthorizationSkyWayFragment();
                break;
            case REGISTRATION_SKY_WAY_FRAGMENT_TAG:
                fragment = new RegistrationSkyWayFragment();
                break;
            case CHAT_CONVERSATION_TAG:
                fragment = new ChatConversationFragment();
                break;
            case CHAT_NEW_ROOM_TAG:
                fragment = new ChatNewGroupFragment();
                break;
            case CHAT_USERS_LIST_TAG:
                fragment = new UserListFragment();
                break;
        }

        fragmentTransaction.replace(R.id.main_activity_container, fragment);
        fragmentTransaction.addToBackStack(fragmentTag);
        fragmentTransaction.commit();
        // set the toolbar title
        getSupportActionBar().setTitle(fragmentTag);
    }

    public void setFragmentTitle(String fragmentTag) {
        getSupportActionBar().setTitle(fragmentTag);

    }

    public String getChatUserName() {
        return chatUserName;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public void setChatUserPassword(String chatUserPassword) {
        this.chatUserPassword = chatUserPassword;
    }

    //Return ArrayList of UserModel
    public boolean parseUserList(Object obj) throws JSONException {
        boolean result = false;
        JSONArray array;
        if (obj != null) {
            userList = new ArrayList<>();
            try {
                array = (JSONArray) obj;
            } catch (Exception e) {
                Log.e(TAG, "(JSONArray)obj " + e.toString());
                return false;
            }
            JSONObject json;
            for (int i = 0; i < array.length(); i++) {
                json = (JSONObject) array.get(i);
                UserModel user = new UserModel();
                user.setId(json.getString("id"));
                user.setNickname(json.getString("nickname"));
                user.setConnected(json.getBoolean("isConnected"));
                userList.add(user);
                if (chatUserName.equals(user.getNickname())) chatUserId = user.getId();
            }
            result = true;
        } else {
            Log.e(TAG, "parseUserList array == null");
        }
        return result;
    }


    //Return ArrayList of UserModel
    public ArrayList<GroupTabModel> parseGroupsList(Object obj) throws JSONException {
        ArrayList<GroupTabModel> result = new ArrayList<>();
        JSONArray array;
        if (obj != null) {
            try {
                array = (JSONArray) obj;
            } catch (Exception e) {
                Log.e(TAG, "(JSONArray)obj " + e.toString());
                return result;
            }
            JSONObject json;
            for (int i = 0; i < array.length(); i++) {
                json = (JSONObject) array.get(i);
                GroupTabModel room = new GroupTabModel();
                room.setRoomName(json.getString("roomName"));
                room.setRoomDescription(json.getString("roomDescription"));
                room.setRoomIconUrl(json.getString("roomIconUrl"));
                result.add(room);
            }
        } else {
            Log.e(TAG, "parseUserList array == null");
        }
        return result;
    }


    //Return ArrayList of UserModel
    public ArrayList<PrivateTabModel> parsePrivateGroupsList(Object obj) throws JSONException {
        ArrayList<PrivateTabModel> result = new ArrayList<>();
        JSONArray array;
        if (obj != null) {
            try {
                array = (JSONArray) obj;
            } catch (Exception e) {
                Log.e(TAG, "(JSONArray)obj " + e.toString());
                return result;
            }
            JSONObject json;
            for (int i = 0; i < array.length(); i++) {
                json = (JSONObject) array.get(i);
                PrivateTabModel room = new PrivateTabModel();
                room.setRoomName(json.getString("roomName"));
                room.setRoomNick1(json.getString("roomNick1"));
                room.setRoomNick2(json.getString("roomNick2"));
                result.add(room);
            }
        } else {
            Log.e(TAG, "parseUserList array == null");
        }
        return result;
    }


    //Return ArrayList of MessageModel
    public ArrayList<MessageModel> parseChatHistory(Object obj) throws JSONException {
        ArrayList<MessageModel> result = new ArrayList<>();
        JSONArray array;
        if (obj != null) {
            try {
                array = (JSONArray) obj;
                if (isLog) Log.v("flow", obj.toString());
            } catch (Exception e) {
                Log.e(TAG, "(JSONArray)obj " + e.toString());
                return result;
            }
            JSONObject json;

            for (int i = array.length() - 1; i >= 0; i--) {
                json = (JSONObject) array.get(i);
                if ((isLog) && (isChatHistoryLog)) Log.v("flow", "Item: " + json.toString());
                result.add(parseMessageFromJSON(json));
            }

        } else {
            Log.e(TAG, "parseUserList array == null");
        }
        return result;
    }


    public MessageModel parseMessageFromJSON(JSONObject json) {
        MessageModel mes = new MessageModel();
        try {
            mes.setClientNickname(json.getString("clientNickname"));
            mes.setMsg(json.getString("msg"));
            mes.setCurrentDateTime(json.getString("currentDateTime"));
            mes.setMessageType(json.getString("messageType"));
            if (!json.isNull("room")) mes.setRoom(json.getString("room"));
        } catch (Exception e) {
            Log.e(TAG, "Error in parsing JSON Message" + e.toString());
        }
        return mes;
    }

    public boolean isRoomPrivate() {
        return isRoomPrivate;
    }

    public void setRoomPrivate(boolean roomPrivate) {
        isRoomPrivate = roomPrivate;
    }

    public String getCurrentChatRoom() {
        return currentChatRoom;
    }

    public void setCurrentChatRoom(String currentChatRoom) {
        this.currentChatRoom = currentChatRoom;
    }

    public ArrayList<UserModel> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<UserModel> userList) {
        this.userList = userList;
    }

    public ArrayList<PrivateTabModel> getPrivateRoomList() {
        return privateRoomList;
    }

    public boolean isNetConnection() {
        //to do check internet connection
        return true;
    }

    public void setUserType(boolean userType) {
        this.userType = userType;
    }


    public boolean isUserType() {
        return userType;
    }


}
