package com.a256devs.skyway360.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.a256devs.skyway360.Dialogs.SetTitleDialog;
import com.a256devs.skyway360.EventBus.MessageEvent;
import com.a256devs.skyway360.Models.ImageUploadResponse;
import com.a256devs.skyway360.Models.LoadCoverResponse;
import com.a256devs.skyway360.R;
import com.a256devs.skyway360.REST.ApiRequests;
import com.squareup.picasso.Picasso;
import com.sw926.imagefileselector.ImageFileSelector;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.a256devs.skyway360.EventBus.Messages.IMAGE_FOR_NEW_GROUP;
import static com.a256devs.skyway360.EventBus.Messages.MENU_OK_PRESSED;
import static com.a256devs.skyway360.EventBus.Messages.NEW_GROUP_CHAT_MESSAGE;
import static com.a256devs.skyway360.EventBus.Messages.NEW_PRIVATE_CHAT_MESSAGE;
import static com.a256devs.skyway360.EventBus.Messages.NEW_ROOM_CREATED;
import static com.a256devs.skyway360.EventBus.Messages.SOCKET_CONNECTED;
import static com.a256devs.skyway360.REST.UploadMethods.uploadCover;
import static com.a256devs.skyway360.utils.CallBacks.setImageViewCallBack;

public class MainActivity extends SettingsActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    String TAG = "MainActivity";
    SetTitleDialog setTitleDialog;
    Menu mMenu;
    ProgressDialog progressDialog;
    SharedPreferences sharedPref;
    public SharedPreferences.Editor editor;
    private Socket mSocket;
    private boolean isSocketConnected = false;
    private boolean isUserConnected = false;

    //Drawer UI elements
    Button mNavEditImg, mNavEditTitle, mNavSaveButton;
    ImageView mDrawerImageView;
    TextView mDrawerTextView;
    ImageFileSelector mImageFileSelector;
    com.a256devs.skyway360.ui.AutoResizeTextView mDrawerActionTextView;


    public String newGroupFilePath;

    //UploadingCoverSection
    boolean isCover = false;
    String newCoverFilePath;
    private String coverTitleString;
    private String description2;


    //Create new groupSection
    public boolean isNewGroup = false;


    //fab visibility
    boolean fabVisibility = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        showFragmentWithoutBack(NEWS_FRAGMENT_TAG);
        sharedPref = getSharedPreferences(getString(R.string.app_preferences), Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        View header = navigationView.getHeaderView(0);

        mNavEditImg = (Button) header.findViewById(R.id.nav_edit_img);
        mNavEditTitle = (Button) header.findViewById(R.id.nav_edit_title);
        mNavSaveButton = (Button) header.findViewById(R.id.nav_save_button);
        mDrawerImageView = (ImageView) header.findViewById(R.id.drawer_image_iv);
        mDrawerTextView = (TextView) header.findViewById(R.id.drawer_text_tv);
        mDrawerActionTextView = (com.a256devs.skyway360.ui.AutoResizeTextView) navigationView.findViewById(R.id.drawer_action_text_tv);


        mNavEditImg.setOnClickListener(this);
        mNavEditTitle.setOnClickListener(this);
        mNavSaveButton.setOnClickListener(this);

        setTitleDialog = new SetTitleDialog();

        mImageFileSelector = new ImageFileSelector(this);
        mImageFileSelector.setCallback(setImageViewCallBack());

        setSocketServerURL();
        setSettingsVisibility(false);
        if (isNetConnection()) loadCover();
        EventBus.getDefault().register(this);

        if (isCurrentUserAdmin()) setSettingsVisibility(true);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        mMenu = menu;

        return true;
    }

    @Override
    protected void onStart() {
        if (isLog) Log.v("flow", "MainActivity: onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        if (isLog) Log.v("flow", "MainActivity: onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (isLog) Log.v("flow", "MainActivity: onDestroy");
        EventBus.getDefault().unregister(this);
        if (isUserConnected) disconnectUser();
        if (isSocketConnected) disconnectSocket();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            makeLogout();
            return true;
        } else if (id == R.id.users_list) {
            showFragment(CHAT_USERS_LIST_TAG);
            return true;
        } else if (id == R.id.ok) {
            EventBus.getDefault().post(new MessageEvent(MENU_OK_PRESSED, null));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_news) {
            showFragment(NEWS_FRAGMENT_TAG);
        } else if (id == R.id.nav_chat) {
            if (tryToGetUserNameToMainActivity()) {
                showFragment(CHAT_FRAGMENT_TAG);
            } else {
                showFragment(CHAT_AUTHORIZATION_FRAGMENT_TAG);
            }
        } else if (id == R.id.nav_authorization) {
            showFragment(AUTHORIZATION_SKY_WAY_FRAGMENT_TAG);
        } else if (id == R.id.nav_registration) {
            showFragment(REGISTRATION_SKY_WAY_FRAGMENT_TAG);
        } else if (id == R.id.nav_feedback) {
            showFragment(FEEDBACK_FRAGMENT_TAG);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean isCurrentUserAdmin() {
        if (sharedPref.getBoolean("userType", false)) return true;
        else return false;
    }

    public boolean tryToGetUserNameToMainActivity() {
        chatUserName = sharedPref.getString("shared_user_name", null);
        chatUserPassword = sharedPref.getString("shared_password", null);
        if ((chatUserName != null) && (chatUserPassword != null)) {
            return true;
        }
        return false;
    }

    public void saveUserNameAndPasswordToShared() {
        editor.putString("shared_user_name", chatUserName);
        editor.putString("shared_password", chatUserPassword);
        editor.putBoolean("userType", userType);
        if (isLog) Log.v("flow", "Saved userType into the sharedPref:" + userType);
        editor.commit();
    }

    public void makeLogout() {
        setSettingsVisibility(false);
        if (isUserConnected) disconnectUser();
        chatUserName = null;
        chatUserPassword = null;
        editor.remove("shared_user_name");
        editor.remove("shared_password");
        editor.remove("userType");
        editor.commit();
        disconnectSocket();
        showFragment(CHAT_AUTHORIZATION_FRAGMENT_TAG);
    }

    public void showMenuItems(String fragmentTag, boolean visibility) {
        if ((fragmentTag != null) && (mMenu != null))
            switch (fragmentTag) {
                case NEWS_FRAGMENT_TAG:
                    break;
                case CHAT_FRAGMENT_TAG:
                    mMenu.findItem(R.id.action_logout).setVisible(visibility);
                    break;
                case CHAT_CONVERSATION_TAG:
                    mMenu.findItem(R.id.action_logout).setVisible(visibility);
                    mMenu.findItem(R.id.users_list).setVisible(visibility);
                    break;
                case CHAT_AUTHORIZATION_FRAGMENT_TAG:
                    break;
                case CHAT_REGISTRATION_FRAGMENT_TAG:
                    break;
                case FEEDBACK_FRAGMENT_TAG:
                    break;
                case AUTHORIZATION_SKY_WAY_FRAGMENT_TAG:
                    break;
                case REGISTRATION_SKY_WAY_FRAGMENT_TAG:
                    break;
                case CHAT_NEW_ROOM_TAG:
                    mMenu.findItem(R.id.ok).setVisible(visibility);
                    break;
            }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nav_edit_img:
                selectImageForCover();

                break;
            case R.id.nav_edit_title:
                if (!setTitleDialog.isAdded())
                    setTitleDialog.show(getFragmentManager(), "Set title dialog");
                break;
            case R.id.nav_save_button:
                uploadCover(newCoverFilePath, coverTitleString, description2);
                break;
        }
    }


    private Emitter.Listener onUserList = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        parseUserList(args[0]);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isLog) Log.v("flow", "in on Connect Listener");
                    isSocketConnected = true;
                    EventBus.getDefault().post(new MessageEvent(SOCKET_CONNECTED, null));
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isSocketConnected = false;
                    if (isLog) Log.v("flow", "in onConnectError");

                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isSocketConnected = false;
                    if (isLog) Log.v("flow", "in onConnectError");

                }
            });
        }
    };

    private Emitter.Listener newRoom = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Object obj = args[0];
                    if (isLog) Log.v("flow", "New room " + obj.toString());
                    EventBus.getDefault().post(new MessageEvent(NEW_ROOM_CREATED, null));
                }
            });
        }
    };

    private Emitter.Listener newMessageNottification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Object obj = args[0];
                    if (isLog) Log.v("flow", "New message in the room:  " + obj.toString());
                    JSONObject jsonObject;
                    jsonObject = (JSONObject) obj;
                    String roomWithMessage = "";
                    try {
                        roomWithMessage = jsonObject.getString("room");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (!roomWithMessage.equals("")) {
                        if (groupsRoomList != null)
                            for (Integer i = 0; i < groupsRoomList.size(); i++)
                                if (groupsRoomList.get(i).getRoomName().equals(roomWithMessage)) {
                                    groupsRoomList.get(i).setNewMessages(true);
                                    EventBus.getDefault().post(new MessageEvent(NEW_GROUP_CHAT_MESSAGE, i));
                                }
                        if (privateRoomList != null)
                            for (Integer i = 0; i < privateRoomList.size(); i++)
                                if (privateRoomList.get(i).getRoomName().equals(roomWithMessage)) {
                                    privateRoomList.get(i).setNewMessages(true);
                                    EventBus.getDefault().post(new MessageEvent(NEW_PRIVATE_CHAT_MESSAGE, i));
                                }
                    }
                }
            });
        }
    };


    public void setSettingsVisibility(Boolean visibility) {
        fabVisibility = visibility;
        if (visibility) {
            mNavEditImg.setVisibility(View.VISIBLE);
            mNavEditTitle.setVisibility(View.VISIBLE);
            mNavSaveButton.setVisibility(View.VISIBLE);
        } else {
            mNavEditImg.setVisibility(View.INVISIBLE);
            mNavEditTitle.setVisibility(View.INVISIBLE);
            mNavSaveButton.setVisibility(View.INVISIBLE);
        }
    }

    public void refreshPrivateTab() {
        if (isLog) Log.v("flow", "in refreshPrivateTab" + " TAG:" + TAG);
        mSocket.emit("roomList", "", chatUserName);

    }

    public void refreshGroupsTab() {
        if (isLog) Log.v("flow", "in refreshGroupsTab" + " TAG:" + TAG);
        mSocket.emit("roomList", "", "");
    }

    public void selectImageForChat() {
        if (isLog) Log.v("flow", "in selectImageForChat");
        isNewGroup = false;
        isCover = false;
        mImageFileSelector.selectImage(this, 1);
    }

    public void selectImageForNewChat() {
        isNewGroup = true;
        isCover = false;
        mImageFileSelector.selectImage(this, 2);
    }

    public void selectImageForCover() {
        isCover = true;
        isNewGroup = false;
        mImageFileSelector.selectImage(this, 3);
    }

    public void showProgressDialog(String mes) {
        progressDialog = ProgressDialog.show(this, "", mes, true);
    }

    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    private void startImageUploading(String file) {
        if (isNetConnection()) {
            progressDialog = ProgressDialog.show(this, "", "Please wait, Uploading Image ... ", true);
            uploadFile(file);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (isLog) Log.v("flow", "MainActivity: onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        mImageFileSelector.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (isLog) Log.v("flow", "MainActivity: onSaveInstanceState");
        super.onSaveInstanceState(outState);
        mImageFileSelector.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (isLog) Log.v("flow", "MainActivity: onRestoreInstanceState");

        super.onRestoreInstanceState(savedInstanceState);
        mImageFileSelector.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (isLog) Log.v("flow", "MainActivity: onRequestPermissionsResult");

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImageFileSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void uploadFile(String filePath) {
        // create upload service client

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiRequests api = retrofit.create(ApiRequests.class);

        File file = new File(filePath);

        if (isLog) Log.v("flow", "file.toString()" + file.toString());
        if (isLog) Log.v("flow", " Absolute path:" + file.getAbsolutePath());

        // create RequestBody instance from file
        final RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        // add another part within the multipart request
        String descriptionString = "file";
        RequestBody description =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), descriptionString);

        // finally, execute the request
        Call<ImageUploadResponse> call = api.upload(description, body);
        call.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call,
                                   Response<ImageUploadResponse> response) {
                progressDialog.dismiss();
                if (isLog) Log.v("flow", "success" + response.body().toString());
                if (response.isSuccessful()) {
                    ImageUploadResponse iR = response.body();
                    if ((iR.getSuccess() != null) && (iR.getSuccess() == 1)) {
                        if (iR.getUrl() != null) {
                            Log.v("flow", "Send message: " + getChatUserName() + iR.getUrl() + getCurrentChatRoom());
                            mSocket.emit("chatMessage", getChatUserName(), iR.getUrl(), getCurrentChatRoom(), "photo");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Image didn't uploaded", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "response isSuccessful for ImageUploadResponse not success");
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Image didn't uploaded", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "response for ImageUploadResponse not success");
                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Image didn't uploaded Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("Upload error:", t.getMessage());
            }
        });
    }


    //Socket methods
    public void setSocketServerURL() {
        {
            try {
                mSocket = IO.socket(CHAT_SERVER_URL);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }
        if (isLog) Log.v("flow", "mSocket toString: " + mSocket.toString());
    }

    public Socket getSocket() {
        return mSocket;
    }

    public void connectSocket() {
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("userList", onUserList);
        mSocket.on("newRoom", newRoom);
        mSocket.on("newMessageNottification", newMessageNottification);
        mSocket.connect();
    }

    public void disconnectSocket() {
        if (isLog) Log.v("flow", "in disconnectSocket");
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("userList", onUserList);
        mSocket.off("newRoom", newRoom);
        mSocket.off("newMessageNottification", newMessageNottification);
        isSocketConnected = false;
        mSocket.disconnect();

    }


    public void connectUser() {
        if (isLog) Log.v("flow", "in connectUser");
        if (isUserConnected) {
            if (isLog) Log.v("flow", TAG + "mes: User already connected");
        } else {
            if (isLog) Log.v("flow", " connectUser : getChatUserName");
            isUserConnected = true;
            mSocket.emit("connectUser", getChatUserName());
        }
    }

    public void disconnectUser() {
        if (isLog) Log.v("flow", "in disconnectUser");
        mSocket.emit("exitUser", getChatUserName());
        isUserConnected = false;
    }

    public void createNewPrivateRoom(String nik2) {
        if (isLog)
            Log.v("flow", "CurrentUser: " + chatUserName + ";  Private chat user: " + nik2);
        String newChatName = "privateRoom" + chatUserName + nik2;
        if (chatUserName.compareToIgnoreCase(nik2) > 0) {
            newChatName = "privateRoom" + nik2 + chatUserName;
            mSocket.emit("newRoom", newChatName, "privateRoom", "", chatUserName, nik2);
        } else {
            mSocket.emit("newRoom", newChatName, "privateRoom", "", nik2, chatUserName);
        }
        if (isLog) Log.v("flow", "Creating newChat with Name: " + newChatName);

        setRoomPrivate(true);
        setCurrentChatRoom(newChatName);
        showFragment(CHAT_CONVERSATION_TAG);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.message) {
            case FILE_FOR_CHAT:
                if (isLog) Log.v("flow", "Main activity: onMessageEvent: FILE_FOR_CHAT");
                if (isCover) {
                    newCoverFilePath = (String) event.link;
                    if (isLog) Log.v("flow", "in the MainAct FILE_FOR_CHAT case; isCover == true");
                    if ((mDrawerImageView != null) && (newCoverFilePath != null)) {
                        if (isLog)
                            Log.v("flow", "in the MainAct FILE_FOR_CHAT case; trying to load cover");
                        Picasso.with(getBaseContext()).load("file://" + newCoverFilePath).into(mDrawerImageView);
                    }
                } else if (isNewGroup) {
                    if (isLog)
                        Log.v("flow", "in the MainAct FILE_FOR_CHAT isNewGroup = true and event.link: " + (String)event.link);
                    newGroupFilePath = (String) event.link;

                } else startImageUploading((String) event.link);

                break;
            case MAKE_LONG_TOAST:
                Toast.makeText(getApplicationContext(), (String) event.link, Toast.LENGTH_LONG).show();
                break;
            case MAKE_SHORT_TOAST:
                Toast.makeText(getApplicationContext(), (String) event.link, Toast.LENGTH_SHORT).show();
                break;
        }

    }

    public boolean isSocketConnected() {
        return isSocketConnected;
    }


    public void loadCover() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiRequests api = retrofit.create(ApiRequests.class);
        Call call = api.loadCover();
        call.enqueue(new Callback<LoadCoverResponse>() {
                         @Override
                         public void onResponse(Call<LoadCoverResponse> call, Response<LoadCoverResponse> response) {
                             if (response.isSuccessful()) {
                                 LoadCoverResponse cover = response.body();
                                 if ((mDrawerTextView != null) && (cover.getDescription() != null)) {
                                     coverTitleString = cover.getDescription();
                                     mDrawerTextView.setText(coverTitleString);
                                     description2 = cover.getDescription2();
                                     mDrawerActionTextView.setText(description2);

                                 } else
                                     Log.e(TAG, "! (mDrawerTextView != null) && (cover.getDescription() != null)");
                                 if ((mDrawerImageView != null) && (cover.getImage() != null))
                                     Picasso.with(getBaseContext()).load(cover.getImage()).into(mDrawerImageView);
                                 else
                                     Log.e(TAG, "! (mDrawerImageView != null) && (cover.getImage()) != null)");
                             } else {
                                 Log.e("flow", "Load cover response not success");
                             }
                         }

                         @Override
                         public void onFailure(Call<LoadCoverResponse> call, Throwable t) {
                             Log.e("flow", "Load cover  Failure " + t.toString());
                         }
                     }
        );
    }

    public String getCoverTitleString() {
        return coverTitleString;
    }

    public void setCoverTitleString(String coverTitleString) {
        this.coverTitleString = coverTitleString;
        mDrawerTextView.setText(coverTitleString);
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        mDrawerActionTextView.setText(description2);
        this.description2 = description2;
    }

    public boolean isFabVisibility() {
        return fabVisibility;
    }

    public void setFabVisibility(boolean fabVisibility) {
        this.fabVisibility = fabVisibility;
    }
public void setnewGroupFilePathToNull(){
    newGroupFilePath = null;
}
}
