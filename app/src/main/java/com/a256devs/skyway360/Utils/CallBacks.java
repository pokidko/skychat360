package com.a256devs.skyway360.utils;


import android.util.Log;

import com.a256devs.skyway360.EventBus.MessageEvent;
import com.sw926.imagefileselector.ErrorResult;
import com.sw926.imagefileselector.ImageFileSelector;

import org.greenrobot.eventbus.EventBus;

import static com.a256devs.skyway360.Activity.SettingsActivity.isLog;
import static com.a256devs.skyway360.EventBus.Messages.FILE_FOR_CHAT;

public class CallBacks {
    static String TAG = "CallBacks Class";

    public static ImageFileSelector.Callback setImageViewCallBack() {
        ImageFileSelector.Callback callback = new ImageFileSelector.Callback() {
            @Override
            public void onSuccess(final String file) {
                if (isLog) Log.v(TAG, " flow setImageViewCallBack and file name is : " + file);
                EventBus.getDefault().post(new MessageEvent(FILE_FOR_CHAT, file));
            }

            @Override
            public void onError(ErrorResult errorResult) {
                switch (errorResult) {
                    case permissionDenied:
                        Log.e(TAG, "flow : setImageViewCallBack error : permissionDenied");

                        break;
                    case canceled:
                        Log.e(TAG, "flow : setImageViewCallBack error : canceled");

                        break;
                    case error:
                        Log.e(TAG, "flow: setImageViewCallBack error : error :"  + errorResult.toString());
                        break;
                }
            }
        };
        return callback;
    }

}
