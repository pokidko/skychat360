package com.a256devs.skyway360.REST;

import com.a256devs.skyway360.Models.ImageUploadResponse;
import com.a256devs.skyway360.Models.LoadCoverResponse;
import com.a256devs.skyway360.Models.LoginResponse;
import com.a256devs.skyway360.Models.RegistrationResponse;
import com.a256devs.skyway360.Models.UploadCoverResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;

public interface ApiRequests {

    @Headers({
            "login:test",
            "password:testp"
    })
    @POST("user/check")
    Call<LoginResponse> login(@QueryMap Map<String, String> options);

    @Headers({
            "login:test",
            "password:testp"
    })
    @POST("user/store")
    Call<RegistrationResponse> register(@QueryMap Map<String, String> options);

    //Load Cover request
    @Headers({
            "login:test",
            "password:testp"
    })
    @POST("settings/get")
    Call<LoadCoverResponse> loadCover();


    @Headers({
            "login:test",
            "password:testp"
    })
    @Multipart
    @POST("user/file/upload")
    Call<ImageUploadResponse> upload(@Part("description") RequestBody description,
                                     @Part MultipartBody.Part file);

    @Headers({
            "login:test",
            "password:testp"
    })
    @Multipart
    @POST("settings/set")
    Call<UploadCoverResponse> uploadCoverImage(@Part("description") RequestBody description1,
                                               @Part("description2") RequestBody description2,
                                               @Part MultipartBody.Part file);


}
