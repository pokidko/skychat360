package com.a256devs.skyway360.REST;


import android.util.Log;
import com.a256devs.skyway360.EventBus.MessageEvent;
import com.a256devs.skyway360.Models.UploadCoverResponse;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.a256devs.skyway360.Activity.SettingsActivity.API_SERVER_URL;
import static com.a256devs.skyway360.Activity.SettingsActivity.isLog;
import static com.a256devs.skyway360.EventBus.Messages.MAKE_LONG_TOAST;

public class UploadMethods {

    public static void uploadCover(String newCoverFilePath, String newCoverTitleString, String newCoverDescription2String) {
        final String TAG = "UploadMethods.uploadCover()";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiRequests api = retrofit.create(ApiRequests.class);
        MultipartBody.Part body = null;
        if (newCoverFilePath != null) {
            File file = new File(newCoverFilePath);
            if (isLog) Log.v("flow", TAG + " newCoverFilePath: " + newCoverFilePath);
            // create RequestBody instance from file
            final RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

            // MultipartBody.Part is used to send also the actual file name
            body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        }
        RequestBody desc1 = RequestBody.create(MediaType.parse("text/plain"), newCoverTitleString);
        RequestBody desc2 = RequestBody.create(MediaType.parse("text/plain"), newCoverDescription2String);


        // finally, execute the request
        Call<UploadCoverResponse> call = api.uploadCoverImage(desc1, desc2, body);
        call.enqueue(new Callback<UploadCoverResponse>() {
            @Override
            public void onResponse(Call<UploadCoverResponse> call,
                                   Response<UploadCoverResponse> response) {
                if (isLog) Log.v("flow", TAG + "success" + response.body().toString());
                if (response.isSuccessful()) {
                    UploadCoverResponse uploadResponse = response.body();
                    if ((uploadResponse.getSuccess() != null) && (uploadResponse.getSuccess())) {
                        String toastMessage = "Cover Uploaded";
                        EventBus.getDefault().post(new MessageEvent(MAKE_LONG_TOAST, toastMessage));
                        Log.v(TAG, "response isSuccessful ((uploadResponse.getSuccess() != null) && (uploadResponse.getSuccess() == 1)) == true ");
                    } else {
                        String toastMessage = "Image didn't uploaded";
                        EventBus.getDefault().post(new MessageEvent(MAKE_LONG_TOAST, toastMessage));
                        Log.e(TAG, "response isSuccessful for ImageUploadResponse not success");
                    }
                } else {
                    String toastMessage = "Image didn't uploaded";
                    EventBus.getDefault().post(new MessageEvent(MAKE_LONG_TOAST, toastMessage));
                    Log.e(TAG, "response for ImageUploadResponse not success");
                }
            }

            @Override
            public void onFailure(Call<UploadCoverResponse> call, Throwable t) {
                String toastMessage = "Image didn't uploaded";
                EventBus.getDefault().post(new MessageEvent(MAKE_LONG_TOAST, toastMessage));
                Log.e("Upload error:", t.getMessage());
            }
        });
    }



}
